from __future__ import absolute_import, print_function
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from bs4 import BeautifulSoup
import psycopg2 # fuer Zugriff auf Datenbank
import requests
import json # damit wir die Tweet Daten lesen und parsen koennen
import csv # um CSV Datei zu oeffnen und schreiben
import os.path # um zu ueberpruefen, ob CSV Datei schon existiert
import time # fuer sleep

class StdOutListener(StreamListener):
    """ A listener handles tweets that are received from the stream.
    This is a basic listener that just prints received tweets to stdout.
    """
    def on_data(self, data):
        decoded = json.loads(data) # Ausgabe von Twitter API wird als JSON dargestellt
        try: # ignoriert alle Retweets
            isRT = decoded['retweeted_status']
            print("This was a retweet! Ignored.")
            pass
        except KeyError:
            user_id = decoded['user']['screen_name']
            text = decoded['text']
            time = decoded['created_at']
            tweet_id = decoded['id']
            hashlist = decoded['entities']['hashtags']
            hashes = []
            for entry in hashlist:
                hashes.append(str(entry['text']))
            print("User: "+str(user_id))
            print("Text: "+str(text))
            print("Posted On: "+str(time))
            print("ID: "+str(tweet_id))
            print("Hashtags: "+str(hashes))
            print("\n")
            tweetdata = (tweet_id,user_id,text,time,hashes)
            fwriter.writerow(list(tweetdata)) # schreibt eine neue Zeile in CSV Datei
            tweet_insertion = """INSERT INTO trumptweets (tweet_id,user_id,text,time,hashes) VALUES (%s, %s, %s, %s, %s)"""
            cursor.execute(tweet_insertion, tweetdata)
            conn.commit() # schreibt eine neue Zeile im Datenbank
            return True        

    def on_error(self, status):
        print(status)
        f.close()

### Benutzer wird gefragt, fuer wie viele Sekunden werden Tweets gesammelt/wird Stream geoeffnet
timeout = 0
while True:
    try:
        while timeout <= 0:
            timeout = int(input("How long (in seconds) do you want to collect tweets? "))
            if timeout <= 0:
                print("Invalid input. Please enter an integer greater than 0!")
        break
    except ValueError:
        print("Invalid input. Please enter an integer greater than 0!")

### Zugriff auf Twitter API
CONSUMER_KEY = '###'
CONSUMER_SECRET = '###'
ACCESS_TOKEN = '###' 
ACCESS_SECRET = '###'

### Falls CSV Datei existiert, dann werden neue Zeile einfach hinzugefuegt, sonst wird die Datei mit dem entsprechenden Header erzeugt
if os.path.isfile('trumptweets.csv'):
    f = open('trumptweets.csv', 'a')
else:
    f = open('trumptweets.csv', 'w')
    head = "TWEET_ID;USER;TEXT;TIMESTAMP;HASHTAGS\n"
    f.write(head)
    
### CSV Writer Objekt erzeugt. Wir nutzen Semikolons als delimiter.
fwriter = csv.writer(f, delimiter=';')

### Um mit dem Datenbank zu verbinden
conn_string = "host='agdbs-edu01.imp.fu-berlin.de' port='5432' dbname='trumptweets' user='student' password='password'" #damit koennen wir eine Verbindung mit unserer Datenbank erstellen
    
try:
    #durch conn_string versuchen wir hier eine Verbindung mit unserer Datenbank zu machen
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor()
except:
    print ("Not connected!")

listener = StdOutListener()
auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
stream = Stream(auth, listener)
stream.filter(track=['@realDonaldTrump'], async=True) # async=True damit wir den Stream schliessen koennen
time.sleep(timeout) # Prozess beendet nach timeout viele Sekunden
stream.disconnect()
cursor.close()
conn.close()
print(str(timeout)+" seconds are over. All done.")